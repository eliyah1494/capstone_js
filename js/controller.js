let renderProductList = function (listSP) {
  let contentHTML = "";
  listSP.forEach(function (phone) {
    contentHTML += `
      <div class="product-item col-3 col-sm- col-md-6 col-lg-3">
      <img src="${phone.img}" />
      <strong class="">${phone.name}</strong>
      <p>${phone.desc}</p>
      <h3>${new Intl.NumberFormat("de-DE").format(phone.price)} </h3>
      <p>Screen: ${phone.screen}</p>
      <p>Back Camera: ${phone.backCamera} </p>
      <p>Front Camera: ${phone.frontCamera}</p>
     <button onclick="addCart('${phone.id}')" class="btn btn-add">Thêm</button>
      </div>`;
  });

  document.getElementById("productList").innerHTML = contentHTML;
};

let saveLocalStorageProduct = () => {
  var productListJson = JSON.stringify(product);
  localStorage.setItem("PRODUCT", productListJson);
};

// function render Cart
let renderCart = (listCart) => {
  var contentHTML = "";
  var sumMoney = 0;
  for (var i = 0; i < listCart.length; i++) {
    var currentCart = listCart[i].phone;
    if (!currentCart) {
      return;
    }
    var productMoney = currentCart.price * listCart[i].quantity;
    var productMoneyFormat = new Intl.NumberFormat("de-DE").format(
      productMoney
    );
    var contentTr = `<tr>
    <hr />
                        <td class="product"><img src="${currentCart.img}" alt="" style="width:100px;height:60px"></td>
                        
                        <td>${currentCart.name}</td>
                        
                        <td>
                            <p><span>${productMoneyFormat}</span><sup>đ</sup></p>
                        </td>                   
                        <td>
                        <input onclick=tru(${currentCart.id}) type="button" value="-" style="padding: 0 5px">
                        <input type="text" value="${listCart[i].quantity}" style="width:40px" id="text-box">
                        <input onclick=cong(${currentCart.id}) type="button" value="+" style="padding: 0 5px">
                        </td>
                        <td>
                        <button onclick="deleteProduct(${currentCart.id})" class="btn btn-success">Xóa</button>
                        </td>
                    </tr>
                    `;
    contentHTML += contentTr;
    sumMoney += productMoney;
  }

  document.getElementById("cart__total").innerHTML = contentHTML;
  document.getElementById("sumMoney").innerHTML = `${new Intl.NumberFormat(
    "de-DE"
  ).format(sumMoney)} VND`;
  renderPay(cart);
};

let renderPay = (listPay) => {
  var contentHTML = "";
  var sumPay = 0;
  for (var i = 0; i < listPay.length; i++) {
    var currentPay = listPay[i].phone;
    var productMoney = currentPay.price * listPay[i].quantity;
    var productMoneyFormat = new Intl.NumberFormat("de-DE").format(
      productMoney
    );
    var contentTr = `<tr>
    <hr />
                        <td class="product"><img src="${currentPay.img}" alt="" style="width:60px;height:40px"></td>
                        <td>${currentPay.name}</td>
                        <td>
                            <p><span>${productMoneyFormat}</span><sup>đ</sup></p>
                        </td>
                        <td>
                        <button onclick="toBack(${currentPay.id})" class="btn btn-success">Quay lại</button>
                        </td>
                    </tr>
                    `;
    contentHTML += contentTr;
    sumPay += productMoney;
  }

  document.getElementById("pay_total").innerHTML = contentHTML;
  document.getElementById("sumPay").innerHTML = `${new Intl.NumberFormat(
    "de-DE"
  ).format(sumPay)} VND`;
};

// save cart local storage
let saveLocalStorageCart = () => {
  var cartListJson = JSON.stringify(cart);
  localStorage.setItem("CART", cartListJson);
};

let clickCart = () => {
  let cartBtn = document.querySelector(".fa-times");
  let clearBtn = document.querySelector(".cart__phone");

  clearBtn.addEventListener("click", function () {
    document.querySelector(".cart").style.right = "0";
  });
  cartBtn.addEventListener("click", function () {
    document.querySelector(".cart").style.right = "-100%";
  });
};
clickCart();

let clickPay = () => {
  let payBtn = document.querySelector(".latch__Order");
  let cartBtn = document.querySelector(".returnCart");

  payBtn.addEventListener("click", function () {
    document.querySelector(".pay").style.left = "50%";
    document.querySelector(".cart").style.right = "-100%";
  });

  cartBtn.addEventListener("click", function () {
    document.querySelector(".cart").style.right = "0";
    document.querySelector(".pay").style.left = "-100%";
  });
};
clickPay();

let completePay = () => {
  let completeBtn = document.querySelector(".complete");

  completeBtn.addEventListener("click", function () {
    document.querySelector(".success").style.top = "50%";
    document.querySelector(".pay").style.left = "-100%";
    resetCart();
  });
};
completePay();

let continueShop = () => {
  let successBtn = document.querySelector(".success__ok");

  successBtn.addEventListener("click", function () {
    document.querySelector(".success").style.top = "-100%";
  });
};
continueShop();
