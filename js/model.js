function ProductList(
  _id,
  _name,
  _price,
  _screen,
  _backcamera,
  _frontcamera,
  _img,
  _desc,
  _type
) {
  this.id = _id;
  this.name = _name;
  this.price = _price;
  this.screen = _screen;
  this.backCamera = _backcamera;
  this.frontCamera = _frontcamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
}
