let product = [];

let dataProductJson = localStorage.getItem("PRODUCT");
if (dataProductJson) {
  product = JSON.parse(dataProductJson);
  renderProductList(product);
}

let cart = [];
let dataCartJson = localStorage.getItem("CART");
if (dataCartJson) {
  cart = JSON.parse(dataCartJson);
  renderCart(cart);
}

// get quantity local storage
var totalQuantityCart = 0;
var dataQuantityCartJson = localStorage.getItem("QUANTITY-CART");
if (dataQuantityCartJson) {
  // truthy falsy
  totalQuantityCart = JSON.parse(dataQuantityCartJson);
  // console.log("totalQuantityCart", totalQuantityCart);
  document.getElementById("total__phone").innerHTML = totalQuantityCart;
}

const BASE_URL = "https://633ec04c83f50e9ba3b7605d.mockapi.io";

axios({
  url: `${BASE_URL}/phone`,
  method: "GET",
})
  .then(function (res) {
    product = res.data;
    // LƯU XUỐNG LOCAL
    saveLocalStorageProduct();

    // renderProductList(res.data);
  })
  .catch(function (err) {});

// Lọc sản phẩm theo type
let choosePhone = (obj) => {
  let value = obj.value;
  if (value == "samSung") {
    var choosePhone = [];
    for (let i = 0; i < product.length; i++) {
      if (product[i].type == "samSung") {
        choosePhone.push(product[i]);
      }
    }
  } else if (value == "iPhone") {
    var choosePhone = [];
    for (let i = 0; i < product.length; i++) {
      if (product[i].type == "iPhone") {
        choosePhone.push(product[i]);
      }
    }
  } else {
    renderProductList(product);
  }
  renderProductList(choosePhone);
};

let addCart = (idPhone) => {
  var indexProduct;
  for (let i = 0; i < product.length; i++) {
    if (product[i].id == idPhone) {
      indexProduct = i;
    }
  }

  var check = false;
  var index;
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].phone.id == idPhone) {
      check = true;
      index = i;
    }
  }

  if (check) {
    cart[index].quantity = cart[index].quantity + 1;
  } else {
    let cartItem = { phone: product[indexProduct], quantity: 1 };
    cart.push(cartItem);
  }

  saveLocalStorageCart();
  renderCart(cart);
  renderPay(cart);
  totalCart();
};

let cong = (idPhone) => {
  console.log("idPhone", idPhone);
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].phone.id == idPhone) {
      cart[i].quantity = cart[i].quantity + 1;
    }
  }
  saveLocalStorageCart();
  renderCart(cart);
  renderPay(cart);
  totalCart();
};

// xíu đổi tên bằng tiếng anh nha ok
let tru = (idPhone) => {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].phone.id == idPhone) {
      cart[i].quantity = cart[i].quantity - 1;
    }

    if (cart[i].quantity <= 0) {
      cart.splice(i, 1);
    }
  }
  saveLocalStorageCart();
  renderCart(cart);
  renderPay(cart);
  totalCart();
};

let deleteProduct = (idPhone) => {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].phone.id == idPhone) {
      cart.splice(i, 1);
    }
  }
  saveLocalStorageCart();
  renderCart(cart);
  renderPay(cart);
  totalCart();
};

let resetCart = () => {
  if (cart != null) {
    cart.length = 0;
  }
  saveLocalStorageCart();
  renderCart(cart);
  renderPay(cart);
  totalCart();
};

let totalCart = () => {
  let totalCart = 0;
  for (let index = 0; index < cart.length; index++) {
    totalCart += cart[index].quantity;
  }

  document.getElementById("total__phone").innerHTML = totalCart;
  // Lưu total quantity Cart xuống localStorage.
  var cartQuantityJson = JSON.stringify(totalCart);
  localStorage.setItem("QUANTITY-CART", cartQuantityJson);
  // console.log("QUANTITY-CART:", cartQuantityJson);

  return totalCart;
};
